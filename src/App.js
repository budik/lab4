// import logo from './logo.svg';
// import './App.css';

import React from 'react';
import ContextDemo from "./tasks/task2/index";
import RefDemo from "./2_Ref";
import HooksRoot from "./5_Hooks";
// import HooksRoot from "./4_HOC";
//imp


const App = () => {
  return (
    <div className="App">
      <ContextDemo>
      </ContextDemo>
      {/*<RefDemo/>*/}

      {/*<HooksRoot/>*/}

    </div>
  );
}

export default App;
