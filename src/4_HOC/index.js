import React, { Component } from 'react'


const LocalStorageHOC = ( Component ) => {
    let data = localStorage.getItem('HOC');
    return () => {
        return(
            <Component message={data} />
        );
    }
}

// localStorage.setItem('HOC', "Hello World");
//
const Message = ({ message }) => (<h2>{ message }</h2>);
const Row = ({ message }) => (<div className="row">{ message }</div>);
//
const HOCMessage = LocalStorageHOC( Message );
const HOCRow = LocalStorageHOC( Row );


export default class HighOrderComponentDemo extends Component {


    method = ( id ) => ( e ) => {
        //...
    }

    render() {
        return (
            <div>
                <h1> HOC </h1>

                <hr/>
                <Message message="One"/>
                <Row message="Two"/>
                <hr/>
                <HOCMessage />
                <HOCRow />
            </div>
        )
    }
}


/*

    HOF => High Order Function

    function method( id ){
        return function( event ){
            //... id, event
        }
    }

    const method = ( id ) => ( event ) => {
        // ... id, event
    }

    const btn = document.getElementById('btn');
    btn.addEventListener('click', method(125) )

*/