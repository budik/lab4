import React,{Component} from 'react';


export default class RefDemo extends Component{


    inputRef = React.createRef();

    myRefMethod =  (node) => {
        console.log(node);
    }


    componentDidMount = () =>  {

        console.log('onmount ' ,this.inputRef);
    }

    render() {

        console.log('inputRef',this.inputRef);
        return(
            <div>
            <h1>Ref Demo</h1>
                <div
                ref={this.inputRef}
                // ref={node => this.myRefMethod(node)}>
            >
            </div>
            </div>

        );
    }
}