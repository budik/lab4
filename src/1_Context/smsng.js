import React from 'react';
import {Button} from "./button";
import {ThemeContext} from "./context_header";


export const SomeThing = () => {

    return(
        <div>
            <h2>
               Something
            </h2>
            <ThemeContext.Consumer>
                {
                    (value) => {
                        console.log('value',value);
                        return(
                            <Button action={value.changeTheme('lime')}>Lime</Button>
                        )
                    }
                }

            </ThemeContext.Consumer>


        </div>
    )


};