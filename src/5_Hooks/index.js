import React from 'react'

import UseContextDemo from './useContext/base';

import BaseUseStateDemo from './useState/base';
import TodoUseState from './useState/todo';

import BaseUseEffect from './useEffect/base';
import CustomHook from './useCustomHook';

/*
    useState,
    useEffect,
    useContext,
    customHooks
*/

const HooksRoot = () => {

    return(
        <div>
            <h1> Hooks </h1>

             <UseContextDemo />

            {/* <BaseUseStateDemo /> */}
            {/* <TodoUseState /> */}

            {/* <BaseUseEffect /> */}

            {/*<CustomHook />*/}
        </div>
    )

}


export default HooksRoot;