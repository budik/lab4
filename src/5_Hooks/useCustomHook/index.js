import React, { useState, useEffect } from 'react'


const useDataFetch = ( url ) => {
    const [ data, setData ] = useState([]);
    useEffect( () => {
        fetch( url )
            .then( res => res.json() )
            .then( data  => {
                console.log( data );
                setData( data );
            });
    }, [ url ] );
    return data;
}


const UsersList = () => {
    const users = useDataFetch('https://jsonplaceholder.typicode.com/users');
    return(
        <>
            <h3> Users </h3>
            <ul>
                {
                    users.map( user => (
                        <li key={user.id}>{user.name}</li>
                    ))
                }
            </ul>
        </>
    );
}

const PostsList = () => {
    const posts = useDataFetch('https://jsonplaceholder.typicode.com/posts?limit=10');
    return(
        <>
            <h3> Posts </h3>
            <ul>
                {
                    posts.map( post => (
                        <li key={post.id}>{post.title}</li>
                    ))
                }
            </ul>
        </>
    );
}


const RootComponent = () => {
    return(
        <div>
            <h2>Custom Hook</h2>

            <UsersList />
            <PostsList />
        </div>
    );
}


export default RootComponent;