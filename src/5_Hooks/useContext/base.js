import React, { useContext } from 'react'


export const TitleContext = React.createContext('Title Context');
export const ThemeContext = React.createContext({ theme: "red", changeTheme: () => {} });

const FnContext = () => {
    const TitleData = useContext( TitleContext );
    const ThemeData = useContext( ThemeContext );

    return(
        <div className={ThemeData.theme}>
            <h2> useContext </h2>
            <h3>{ TitleData } </h3>
        </div>
    )

}


export default FnContext;