import React, { useState } from 'react'


/*

    const data = [ 1, 2, 3, 4, 5]

    const [ one, two ] = data;

    useState = ( defaultValue ) => {
        //...
        return [
            value,
            valueSetter
        ]
    } 

*/


const BaseUseState = () => {

    const [ counter, setCounter ] = useState( 0 ); 

    const add = () => setCounter( counter + 1 );
    const sub = () => setCounter( counter - 1 );

    return(
        <div>
            <button onClick={add}> + </button>
            <h2> Counter: {counter}</h2>
            <button onClick={sub}> - </button>
        </div>
    )
}


export default BaseUseState;
