import React, { useState } from 'react'

const HookTodo = () => {

    const [ todoMessage, setTodoMessage ] = useState('');
    const [ todos, setTodos ] = useState([]);

    const addTodo = () => {

        const newTodo = {
            done: false,
            message: todoMessage
        }

        setTodos([...todos, newTodo ]);
        setTodoMessage("");
    }

    const setDone = ( message ) => _ => {
        let changedTodos = todos.map( todo => {
            if( todo.message === message ){
                todo.done = !todo.done;
            }
            return todo;
        });

        setTodos( changedTodos );
    }


    return(
        <div>
            <h2> Hook todo </h2>
            <header>
                <input
                    value={todoMessage}
                    onChange={ ( e ) => setTodoMessage(e.target.value ) }
                />
                <button onClick={addTodo}> Save </button>
            </header>

            <ul>
                {
                    todos.map( todo => (

                        <li 
                            key={todo.message} 
                            className={ todo.done ? "todo__item todo__done" : "todo__item todo__failed"}
                            onClick={ setDone(todo.message) }
                        >
                            { todo.message }
                        </li>
                    ))
                }
            </ul>
        </div>
    )
}


export default HookTodo;