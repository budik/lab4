import React, { useEffect, useState } from 'react'

const UseEffectDemo = () => {

    const [ counter, setCounter ] = useState( 1 ); 
    const [ post, setPost ] = useState( null );

    useEffect( () => {
        //...
        fetch(`https://jsonplaceholder.typicode.com/posts/${counter}`)
            .then( res => res.json() )
            .then( post => {
                console.log( post );
                setPost( post );
            });

    }, [ counter ] );


    useEffect( () => {
        document.title = post ? post.title : "Loading...";
        return () => {
            document.title = "Default";
        }
    });


    const add = () => setCounter( counter + 1 );


    return(
        <div>
            <h2> useEffect </h2>
            <button onClick={add}> {counter} + </button>

            {
                post && (<h3> { post.title }</h3>)
            }
        </div>
    );
}

export default UseEffectDemo;