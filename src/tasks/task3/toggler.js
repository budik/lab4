import React from 'react'


export const Toggler = ({ children, active, action, label }) => {

    return(
        <div className="toggler">
            <b>{ label }: </b>
            {
                React.Children.count(children) > 0 && (
                    <>
                        {
                            React.Children.map( children, ( childItem ) => {
                                if( React.isValidElement(childItem) ){
                                    return React.cloneElement(
                                        childItem,
                                        {
                                            active: childItem.props.value === active,
                                            action: action( childItem.props.value )
                                        }
                                    );
                                }
                            })
                        }
                    </>
                )
            }
        </div>
    );
}


export const TogglerItem = ({ children, active, action, value }) => {
    return(
        <button
            type="button"
            className={active ? "toggler__item active" : "toggler__item"}
            onClick={ action }
        >{children || value }</button>
    )
}
