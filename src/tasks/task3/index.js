import React, { useState } from 'react'
import Uiniqid from 'uniqid';
import PropTypes from 'prop-types';
import {Toggler, TogglerItem} from "./toggler"; // ES6

const HookForm = () => {
    const uniqid = require('uniqid');
    const todoid = uniqid('todo-');
    const [ todoMessage, setTodoMessage ] = useState({
        name:'what',
        email:'is',
        city:'',
        type:'left'
    });

    console.log('[todoMessage]', todoMessage['type']);
    const [ todos, setTodos ] = useState([]);


    const addTodo = () => {


        const newTodo = todoMessage

        setTodos([...todos, newTodo ]);
        setTodoMessage({  name:'',
            email:''});
    }

    const onSubmit = (e) => {
        e.preventDefault();
        console.log('[state]',todos );
    }

    const changeToggler = value => _ => {
        console.log({ align: value });
        setTodoMessage({'name':todoMessage['name'],'email':todoMessage['email'],'city':todoMessage['city'], 'type':value});
    }

    const inputGroupChangeHandler = (event) => {
        setTodoMessage((prevState) => ({
            ...prevState,
            [event.target.name]: event.target.value
        }));
    }




    return(
        <div>
            <h2> Hook todo </h2>
            <form className="form" onSubmit={onSubmit}>
            <header>
                <label> <b> UserName  </b>
                <input
                    name="name"
                    value={todoMessage['name']}
                    // onChange={( e ) => setTodoMessage({'name':e.target.value,'email':todoMessage['email'],'city':todoMessage['city'], 'type':todoMessage['type']}) }
                    onChange={inputGroupChangeHandler }
                />
                </label>
                <label> <b> Email  </b>
                <input
                    name="email"
                    value={todoMessage['email']}
                    // onChange={( e ) => setTodoMessage({'name':todoMessage['name'],'email':e.target.value,'city':todoMessage['city'], 'type':todoMessage['type']}) }
                    onChange={inputGroupChangeHandler }
                />
            </label>
                <label> <b> City  </b>
                    <select
                        name="city"
                        defaultValue={todoMessage['city']}
                        onChange={inputGroupChangeHandler}
                    >
                        <option value="Kyiv">Kyiv</option>
                        <option value="Lviv">Lviv</option>
                        <option value="Harkiv">Harkiv</option>
                        <option value="Odesa ">Odesa </option>

                    </select>
                </label>
                <label> <b> Type  </b>
                    <Toggler
                        name="type"
                        active={todoMessage['type']}
                             action={changeToggler}
                             label="Align"
                    >
                        <TogglerItem value="left"></TogglerItem>
                        <TogglerItem value="center">Center</TogglerItem>
                        <TogglerItem value="right">Right</TogglerItem>
                    </Toggler>

                </label>



                {/*</label>*/}
                <button onClick={addTodo}> Save </button>

            </header>
            </form>
            <ul>{
                    todos.map( (todo,todoid) => (
                        <li
                            key={todoid}
                            className={todo.done ?  "todo__item todo__done" :  "todo__item todo__failed"}
                        >
                            {JSON.stringify(todo)}
                        </li>
                    ))
            }


            </ul>

        </div>
    )
}


export default HookForm;
