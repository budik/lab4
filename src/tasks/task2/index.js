
import React,{Component} from 'react';
import {TitleContext,ThemeContext} from "./context_header";
import {LevelZero} from "./levels";
import {Button} from "./button";
import {SomeThing} from "./smsng";
import {Toggler, TogglerItem} from "./toggler";

console.log(TitleContext);


class ContextDemo extends Component{

    state = {
        theme:"red"
    }

    changeTheme = theme => _ => {
        this.setState({ theme });
    }

    render() {
        const {theme} =  this.state;
        const{changeTheme} = this;
        return (
            <div>
                <h1>ContextDemo</h1>
                <TitleContext.Provider value="Hello World Context">

                </TitleContext.Provider>
                <ThemeContext.Provider value={
                    {
                        theme,
                        changeTheme
                    }
                }>
                <header>
                    <Button action={changeTheme("red")}>Red</Button>
                    <Button action={changeTheme("green")}>Green</Button>
                    <Button action={changeTheme("blue")}>Blue</Button>
                </header>
                    <SomeThing/>
                    <Toggler active={theme} action={this.changeTheme} label="Align"  name="toggler">
                        <TogglerItem  value="red">Red</TogglerItem>
                        <TogglerItem  value="green">green</TogglerItem>
                        <TogglerItem  value="blue">blue</TogglerItem>
                    </Toggler>
                </ThemeContext.Provider>

            </div>
        )
    }

}

export default ContextDemo;