import React from 'react';
import {ThemeContext} from "./context_header";

export const Toggler  = ({children,active,action,label}) => {

    return(
        <div className="toggler">
            <b>{ label }</b>
            { React.Children.count(children) > 0 && (
                <>{
                    React.Children.map(children, (chilItem) => {
                        if (React.isValidElement(chilItem)) {
                            return React.cloneElement(chilItem,
                                {
                                    active: chilItem.props.value === active,
                                    action: action(chilItem.props.value)
                                }
                            );
                        }
                    })
                }
                </>
            )
            }


        </div>
    )
}

export const TogglerItem = ({children,active,action,value}) => {
// console.log(active,action,value);
    return(
        <ThemeContext.Consumer>
            {
                (value) => (
        <button
            type="button"
            className={`btn ${value.theme}`}
            onClick={action}
        >{children}</button>
                )
            }
        </ThemeContext.Consumer>
    )
}