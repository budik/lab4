import React from 'react';
import './button.css'
// import 'button.css';
import {ThemeContext} from "./context_header";


export const Button = ({children,action}) => {
return(
    <ThemeContext.Consumer>
        {
            (value) => (
                <button
                    className={`btn ${value.theme}`}
                    onClick={action}
                >{children}</button>

            )
        }


    </ThemeContext.Consumer>

);

}


// export default Button;