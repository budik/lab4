import React from 'react';
import {TitleContext} from "./context_header";

export const LevelZero = () => (<LevelOne/>);
export const LevelOne = () => (<LevelTwo/>);
export const LevelTwo = () => (<LevelThree/>);
export const LevelThree = () => (<div>
    <TitleContext.Consumer>
        {
            (value) =>{
                return (<h2>Prop Title : {value}</h2>)

            }
        }
    </TitleContext.Consumer>


</div>);