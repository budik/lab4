import React,{Component} from 'react';
import Chart from 'chart.js';



export default class ChartEl extends Component{
    chartEl = React.createRef();
    //
    // myRefMethod = (node) => {
    //     console.log(node);
    //
    // }
    //
    componentDidMount = () => {
        console.log('[CANVAS]',this.chartEl);
        let ctx = this.chartEl.current.getContext('2d');
        this.chart = new Chart(ctx, {
            type: 'line',
            data:{
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets:[
                    {
                        label: 'My First dataset',
                        data: [0, 10, 5, 2, 20, 30, 45],
                        backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(255, 99, 132)'
                    }

                ]

            },
        })

        // this.canvas.current.innerHTML = "<div><h2>Hello from backend</h2><ul><li>1</li><li>1</li></ul></div>"
        // node.style.background = "red";

    }


    randomize = () => {
        // const data1 = Array.from({length:6}, () => (Math.floor( Math.random() * 100)));
        // console.log('Before:',data1 );
        this.chart.data.datasets[0].data = Array.from({length:6}, () => (Math.floor( Math.random() * 100)));
        // this.chart.data.datasets[1].data = Array.from({length:6}, () => (Math.floor( Math.random() * 100)));
        // console.log( this.chart.data.datasets );
        this.chart.update();
    }




    render() {

        // console.log(this.nodeRef);

        return(
            <div>

                <h1>Chart</h1>
                <button onClick={this.randomize}> Randomize </button>
                <canvas
                    ref={this.chartEl}
                    width="400"
                    height="400"/>
            </div>
        )
    }


}
