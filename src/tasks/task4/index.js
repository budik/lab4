import React,{useEffect,useState} from 'react'

const useDataFetch = (url) => {
    const [data,setData] = useState([]);

    useEffect(() => {
        fetch(url)
            .then(res => res.json())
            .then( data => {
                // console.log(data);
                setData(data);
            })
    },[url]);


    return data;

}





const UsersList = () => {
    const users = useDataFetch('https://jsonplaceholder.typicode.com/users/');
    return (
        <>
            <h3>users</h3>
            <ul>
                {
                    users.map (user =>
                        <li key={user.id}>
                            {user.name}
                        </li>
                    )
                }

            </ul>
        </>
    )
}



const PostsList = () => {
    const posts = useDataFetch('https://jsonplaceholder.typicode.com/posts?limit=10');
    return (
        <>
            <h3>Posts</h3>
            <ul>
                {
                    posts.map (post =>
                        <li key={post.id}>
                            {post.title}
                        </li>
                    )
                }

            </ul>
        </>
    )
}



const CommetnsList = () => {
    const coments = useDataFetch('https://jsonplaceholder.typicode.com/comments');
    return (
        <>
            <h3>Commetns</h3>
            <ul>
                {
                    coments.map (coment =>
                        <li key={coment.id}>
                            {coment.email}
                        </li>
                    )
                }

            </ul>
        </>
    )
}




const HomeRootComponent = () => {

    // const posts = useDataFetch('https://jsonplaceholder.typicode.com/posts/');
    // console.log('render',posts);

    return(
        <div className="row" style={{display: 'flex'}}>

            <h2>Custom Hook</h2>

                <div className="col-md-6">
            <UsersList/>
                    </div>
                    <div className="col-md-6">
            <PostsList/>
                        </div>
            <div className="col-md-6">
                <CommetnsList/>
            </div>
            </div>

    );

}



export default HomeRootComponent
